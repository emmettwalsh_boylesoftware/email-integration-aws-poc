


#Email Integration

Provides REST services to send emails through ESP

Implementation : Node.js micro-services organized and deployed using 'The Serverless Framework' (serverless.com)

##Dev env

- In AWS console create IAM user "email-integration", enable Programmatic access
- attach AdministratorAccess Policy to the user
- Copy the key id and secret for the new user 
- Configure local AWS profile for serverless

    serverless config credentials --provider aws --key <key id> --secret <secret>
    
- api key access set up manually in AWS console foe now - (eventually do as mentioned here in api-keys section: https://serverless.com/framework/docs/providers/aws/events/apigateway/ )
- use mocha and mock-aws for unit testing - see nested test folder in emailRequestListener
- use JMeter for load test - see load-test directory



sample:

curl -H "Accept: application/json"   -H "Content-Type:application/json" -H "x-api-key:9bIKxW5RGy1SdgDd6z9nd3vxBbzcheDn1RmtsRXE"  -X POST --data '{"bla":"foobar"}' https://6onuk72rkj.execute-api.eu-west-1.amazonaws.com/dev/sendEmail