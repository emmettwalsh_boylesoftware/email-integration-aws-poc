'use strict';

var AWS = require('aws-sdk');
var helper = require('sendgrid').mail;
var sqs = new AWS.SQS();

var QUEUE_URL = `https://sqs.${process.env.REGION}.amazonaws.com/${process.env.ACCOUNT_ID}/${process.env.STAGE}-emailtaskqueue`;


function mapEventTypeToSendGridTemplateId(eventType){
	return "todo";
}

module.exports.emailWorker = (event, context, callback) => {

	console.log('in emailWorker:', event);

	var to = event.body.to;
	console.log('Sending an email to:', to);



	var from_email = new helper.Email('test@example.com');
	var to_email = new helper.Email(to);
	var subject = 'Hello from the email-integration project!';
	var content = new helper.Content('text/plain', 'Hello!');
	var mail = new helper.Mail(from_email, subject, to_email, content);

	/** example category added to email to distinguish it **/

	var category = new helper.Category("load-test");
	mail.addCategory(category)  ;

	var sg = require('sendgrid')(process.env.SENDGRID_API_KEY);
	var request = sg.emptyRequest({
		method: 'POST',
		path: '/v3/mail/send',
		body: mail.toJSON(),
	});


	console.log('request is:', request);


	sg.API(request, function(error, response) {
		//console.log(response.statusCode);
		//console.log(response.body);
		//console.log(response.headers);

		if(response.statusCode >= 200 && response.statusCode <= 202){

			sqs.deleteMessage({ QueueUrl: QUEUE_URL, ReceiptHandle: event.ReceiptHandle}, function(err, data) {
				if (err){
					console.log(err, err.stack); // an error occurred
					callback(err); //didnt test yet
				}
				else{
					console.log('message was deleted from SQS queue');
					callback(null, "DONE");
				}

			});
		}else{
			console.log("unexpected statusCode from sendGrid:", response.statusCode);
		}



	});





}