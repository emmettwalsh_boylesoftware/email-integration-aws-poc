
var handler = require('../handler.js');

var should = require('should');
var AWS = require('mock-aws'); // include mock-aws instead of aws-sdk
var describe = require('mocha').describe;



describe('handler', function() {
	describe('emailRequestListener()', function() {

		it('should put event body onto the sqs queue', function(done) {


			var testData = {
				MessageBody: '{"foo":"bar"}',
				QueueUrl: 'https://sqs.undefined.amazonaws.com/undefined/undefined-emailtaskqueue'
			}

			// Mock the aws-sdk method 'sendMessage' to return the test data
			AWS.mock('SQS', 'sendMessage', function(params, callback){

				console.log('params are', params);

				params = params || {};

				params.should.eql(testData);

				callback(null,"");
			});

			// Call the function I'm trying to test
			handler.emailRequestListener({ "body" : '{"foo":"bar"}'}, null, function(err, data) {

				data.should.eql({ statusCode: 200 });
				done();
			});

		});
	});
});