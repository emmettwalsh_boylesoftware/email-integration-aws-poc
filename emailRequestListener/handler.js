'use strict';

var AWS = require('aws-sdk');

var QUEUE_URL = `https://sqs.${process.env.REGION}.amazonaws.com/${process.env.ACCOUNT_ID}/${process.env.STAGE}-emailtaskqueue`;


module.exports.emailRequestListener = (event, context, callback) => {

	var sqs = new AWS.SQS(); //put here so we can use mock-aws

	console.log('in emailRequestListener, queue is ', QUEUE_URL);
	console.log('in emailRequestListener, event is ', event);
	var params = {
		MessageBody: event.body,
		QueueUrl: QUEUE_URL
	};
	sqs.sendMessage(params, function(err,data){
		if(err) {
			console.log('error:',"Fail Send Message" + err);
			callback(err);
		}else{
			console.log('data added to queue:',data.MessageId);
			callback(null, { statusCode: 200 });
		}
	});


};
