'use strict';

var AWS = require('aws-sdk');

var sqs = new AWS.SQS();

var async = require("async");

var lambda = new AWS.Lambda();

var QUEUE_URL = `https://sqs.${process.env.REGION}.amazonaws.com/${process.env.ACCOUNT_ID}/${process.env.STAGE}-emailtaskqueue`;

var EMAIL_WORKER = `${process.env.SERVICE}-${process.env.STAGE}-emailWorker`

function receiveMessages(callback) {

	var numMessagesToRead = 10;

	//console.log('in receiveMessages, about to read ',numMessagesToRead);
	//WaitTimeSeconds : The duration (in seconds) for which the call waits for a message to arrive in the queue before returning
	var params = {
		QueueUrl: QUEUE_URL,
		MaxNumberOfMessages: numMessagesToRead,
		WaitTimeSeconds: 20
	};
	sqs.receiveMessage(params, function(err, data) {
		if (err) {
			console.error(err, err.stack);
			callback(err);
		} else {
			if (data.Messages && data.Messages.length > 0) {
				console.log('Got ',data.Messages.length, ' messages off the queue' );
			}else{
				console.log('Got no messages from queue');
			}
			callback(null, data.Messages);
		}
	});
}


function invokeWorkerLambda(task, callback) {

	console.log('Need to invoke worker for this task..',task);

	//task.Body is a json string
	var payload =  {
		"ReceiptHandle" : task.ReceiptHandle,
		"body" : JSON.parse(task.Body)
	};

	console.log('payload:',payload);

	//using 'Event' means use async (http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Lambda.html#invoke-property)
	//TODO need variable here
	var params = {
		FunctionName: EMAIL_WORKER,
		InvocationType: 'Event',
		Payload: JSON.stringify(payload)
	};
	lambda.invoke(params, function(err, data) {
		if (err) {
			console.error(err, err.stack);
			callback(err);
		} else {
			callback(null, data)
		}
	});

}

function handleSQSMessages(context, callback) {
	//console.log('in handleSQSMessages');
	receiveMessages(function(err, messages) {
		if (messages && messages.length > 0) {
			var invocations = [];
			messages.forEach(function(message) {
				invocations.push(function(callback) {
					invokeWorkerLambda(message, callback)
				});
			});
			async.parallel(invocations, function(err) {
				if (err) {
					console.error(err, err.stack);
					callback(err);
				} else {
					if (context.getRemainingTimeInMillis() > 20000) {
						console.log('there is more time to read more messages for this run of the cron')
						handleSQSMessages(context, callback);
					} else {
						console.log('remaining time in millis:',context.getRemainingTimeInMillis(),' No more time, have to PAUSE')
						callback(null, "PAUSE");   //TODO replace this with a recursive call to lambda itself  e.g. lambda.invoke
					}
				}
			});
		} else {
			callback(null, "DONE");
		}
	});
}

module.exports.emailTaskConsumer = (event, context, callback) => {


	//console.log('in emailTaskConsumer');
	handleSQSMessages(context, callback);

}
